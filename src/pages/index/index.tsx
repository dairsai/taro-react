import { View, Text } from '@tarojs/components'
import { Button ,Icon } from '@antmjs/vantui'
import Taro, { useLoad } from '@tarojs/taro'
import './index.scss'

export default function Index() {
  useLoad(() => {
    console.log('Page loaded.')
  })
  const goPage=((e)=>{
    e.stopPropagation() // 阻止冒泡
    console.log('跳转方法');
    Taro.navigateTo({
      url: '/packageIndex/erIndex/index',
    })
  })
  return (
    <View className='index-page'>
      <Text>Hello world!</Text>
      <Button type="info" onClick={goPage}>危险按钮</Button>
      <Icon name="chat-o" size="32px" className="icon"></Icon>
    </View>
  )
}
